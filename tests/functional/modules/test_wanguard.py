import pytest

pytestmark = [
    pytest.mark.requires_salt_modules("wanguard.example_function"),
]


@pytest.fixture
def wanguard(modules):
    return modules.wanguard


def test_replace_this_this_with_something_meaningful(wanguard):
    echo_str = "Echoed!"
    res = wanguard.example_function(echo_str)
    assert res == echo_str

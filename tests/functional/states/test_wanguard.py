import pytest

pytestmark = [
    pytest.mark.requires_salt_states("wanguard.exampled"),
]


@pytest.fixture
def wanguard(states):
    return states.wanguard


def test_replace_this_this_with_something_meaningful(wanguard):
    echo_str = "Echoed!"
    ret = wanguard.exampled(echo_str)
    assert ret.result
    assert not ret.changes
    assert echo_str in ret.comment

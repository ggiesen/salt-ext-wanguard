import pytest

import saltext.wanguard.runners.wanguard_mod as wanguard_runner


@pytest.fixture
def configure_loader_modules():
    module_globals = {
        "__salt__": {"this_does_not_exist.please_replace_it": lambda: True},
    }
    return {
        wanguard_runner: module_globals,
    }


def test_replace_this_this_with_something_meaningful():
    assert "this_does_not_exist.please_replace_it" in wanguard_runner.__salt__
    assert wanguard_runner.__salt__["this_does_not_exist.please_replace_it"]() is True

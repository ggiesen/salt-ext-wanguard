import pytest
import salt.modules.test as testmod

import saltext.wanguard.modules.wanguard_mod as wanguard_module
import saltext.wanguard.states.wanguard_mod as wanguard_state


@pytest.fixture
def configure_loader_modules():
    return {
        wanguard_module: {
            "__salt__": {
                "test.echo": testmod.echo,
            },
        },
        wanguard_state: {
            "__salt__": {
                "wanguard.example_function": wanguard_module.example_function,
            },
        },
    }


def test_replace_this_this_with_something_meaningful():
    echo_str = "Echoed!"
    expected = {
        "name": echo_str,
        "changes": {},
        "result": True,
        "comment": f"The 'wanguard.example_function' returned: '{echo_str}'",
    }
    assert wanguard_state.exampled(echo_str) == expected

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""
Wanguard util module
"""
import json
import logging
import urllib.parse

import salt.utils.http
import validators

log = logging.getLogger(__name__)

# Standard Wanguard REST API URL
DEFAULT_API_URL = "http://localhost/wanguard-api"

# Prefix that is appended to all log entries
LOG_PREFIX = "wanguard:"


def _validate_opts(opts=None, opts_list=None):  # pylint: disable=C0116
    if isinstance(opts_list, list):
        for opt in opts_list:
            if opt == "api_url":
                if opts.get("api_url"):
                    if not validators.url(opts["api_url"], simple_host=True):
                        log.error(
                            '%s Supplied Wanguard REST API URL "%s" is malformed',
                            LOG_PREFIX,
                            opts.get("vault_url"),
                        )
                        return {
                            "Error": f'Supplied Wanguard REST API URL "{opts.get("api_url")}" is malformed'
                        }
                else:
                    opts["api_url"] = DEFAULT_API_URL
            elif opt == "username":
                if opts.get("username"):
                    if not isinstance(opts["username"], str):
                        log.error('%s Value for "username" must be a string', LOG_PREFIX)
                        return {"Error": 'Value for "username" must be a string'}
                else:
                    log.error("%s No username supplied", LOG_PREFIX)
                    return {"Error": "No username supplied"}
            elif opt == "password":
                if opts.get("password"):
                    if not isinstance(opts["password"], str):
                        log.error('%s Value for "password" must be a string', LOG_PREFIX)
                        return {"Error": 'Value for "password" must be a string'}
                else:
                    log.error("%s No password supplied", LOG_PREFIX)
                    return {"Error": "No password supplied"}

        # Everything should be good, return configuration options
        return opts

    log.error("%s Invalid configuration option specified for validation", LOG_PREFIX)
    return {"Error": "Invalid configuration option specified for validation"}


def _get_headers():  # pylint: disable=C0116
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    return headers


def list_roles(opts=None, role_name=None):
    """
    List all user roles

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    role_name
        Search after role name (string, optional)

    Returns a list of dicts if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    params = {}
    if role_name is not None:
        if not isinstance(role_name, str):
            log.error('%s Value for "role_name" must be a string.', LOG_PREFIX)
            return False
        else:
            params["role_name"] = role_name
    if params:
        params = urllib.parse.urlencode(params)
        params = "?" + params
    else:
        params = ""
    headers = _get_headers()
    api_url = config["api_url"]
    roles_url = f"{api_url}/v1/roles{params}"
    roles_results = []
    roles_ret = salt.utils.http.query(
        roles_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in roles_ret:
        log.error(
            '%s API query failed for "list_roles", status code: %s, error %s',
            LOG_PREFIX,
            roles_ret["status"],
            roles_ret["error"],
        )
        return False
    else:
        roles_results = roles_ret["dict"]
        for i, role in enumerate(roles_results):
            del roles_results[i]["href"]
        return roles_results

    return False


def get_role(opts=None, role_name=None, role_id=None):
    """
    Get the configuration of a role

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    role_name
        Role name (string)

    role_id
        Role ID (integer)

    If both role_name and role_id are supplied, role_name will be used.

    Returns a dict if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    if role_name is not None:
        if not isinstance(role_name, str):
            log.error('%s Value for "role_name" must be a string.', LOG_PREFIX)
            return False
        role_list = list_roles(opts=opts, role_name=role_name)
        if not role_list:
            log.error('%s Role "%s" not found.', LOG_PREFIX, role_name)
            return False
        for i, role in enumerate(role_list):
            if role["role_name"] == role_name:
                role_id = int(role["role_id"])
                break
            if i == len(role_list) - 1:
                log.error('%s Role "%s" not found.', LOG_PREFIX, role_name)
                return False
    elif not isinstance(role_id, int):
        log.error('%s Value for "role_id" must be a postive integer.', LOG_PREFIX)
        return False
    headers = _get_headers()
    api_url = config["api_url"]
    role_url = f"{api_url}/v1/roles/{role_id}"
    role_results = {}
    role_ret = salt.utils.http.query(
        role_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in role_ret:
        log.error(
            '%s API query failed for "get_role", status code: %s, error %s',
            LOG_PREFIX,
            role_ret["status"],
            role_ret["error"],
        )
        return False
    else:
        role_results = role_ret["dict"]
        return role_results

    return False


def create_role(
    opts=None,
    role_name=None,
    description=None,
    south_region=None,
    help_menu=None,
    comments=None,
    allow_device_groups=None,
    allow_ip_groups=None,
    allow_servers=None,
    read_only_dashboards=None,
    dashboards_access_policy=None,
    full_dashboards=None,
    reports_tools=None,
    reports_tools_items=None,
    reports_devices=None,
    reports_dashboards=None,
    reports_ip_addresses=None,
    reports_ip_groups=None,
    reports_servers=None,
    ipzones_access_policy=None,
    ipzones=None,
    responses_access_policy=None,
    responses=None,
    response_actions=None,
    threshold_templates_access_policy=None,
    threshold_templates=None,
    whitelist_templates_access_policy=None,
    whitelist_templates=None,
    scheduled_reports_access_policy=None,
    scheduled_reports=None,
):
    """
    Create a user role

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    role_name
        Name of the role (string)

    description
        Description of the role (string, optional)

    south_region
        Display the South Region in the Wanguard console (one of: "Show", "Hide")

    help_menu
        Display the Help menu in the Wanguard console (one of: "Show", "Hide")

    comments
        Comments associated with the role (string, optional)

    allow_device_groups
        List of device groups to allow access to (string or list, optional. Use "All" for access
        to all device groups)

    allow_ip_groups
        List of IP groups to allow access to (string or list, optional. Use "All" for access
        to all IP groups)

    allow_servers
        List of Wanguard servers to allow access to (string or list, optional. Use "All" for access
        to all Wanguard servers)

    read_only_dashboards
        List of dashboard to allow read-only access to (string or list, optional. Use "All" for
        read-only access to all Wanguard dashboards)

    dashboards_access_policy
        The dashboard access policy. (one of: "No modification allowed",
        "Allow modifications to Full-access Dashboards",
        "Allow modifications to Full-access Dashboards and permit adding new ones")

    full_dashboards
        Dashboards to allow full access to (string or list, optional. Use "All" for
        full access to all Wanguard dashboards, or "None" for no full access.)

    reports_tools
        Show tools reports (one of: "Show", "Hide")

    reports_tools_items
      List of tools reports to grant access to. (one or more of: "Anomalies", "Anomalies Actions",
      "Routing", "Routing Actions", "Firewall", "Firewall Actions", "Flows", "Packets",
      "Packets Actions & Captures", "Packets Actions"; optional)

    reports_devices
        Show device reports (one of: "Show", "Hide")

    reports_dashboards
        Show dashboard reports (one of: "Show", "Hide")

    reports_ip_addresses
        Show IP addresses reports (one of: "Show", "Hide")

    reports_ip_groups
        Show IP groups reports (one of: "Show", "Hide")

    reports_servers
        Show servers reports (one of: "Show", "Hide")

    ipzones_access_policy
        IP zones access policy (one of: "No access",
        "Allow full access to the allowed IP Group(s) in the selected IP Zone(s)")

    ipzones
        IP zones to grant access to (string or list, optional)

    responses_access_policy
        Responses access policy (one of: "No access",
        "Allow using the selected Response(s) when defining thresholds",
        "Allow full access to the selected Response(s)",
        "Allow full access to the selected Response(s) and permit adding new ones")

    responses
        Responses to grant access to (string or list, optional)

    response_actions
        Response actions to grant access to (one or more of:
        "Announce the destination IP address in a BGP routing update",
        "Capture a sample of malicious packets",
        "Detect filtering rules and mitigate the attack with Wanguard Filter",
        "Execute a command or script with dynamic parameters as arguments",
        "Send a custom email notification", "Send a custom Syslog message",
        "Send a custom SNMP trap",
        "Send a visual or audio notification to all logged-in Console users",
        "Generate an anomaly report and send it by email",
        "Announce a BGP routing update with Flowspec or S/RTBH",
        "Apply the filtering rule on a third-party inline device",
        "Capture a sample of packets matched by the filtering rule"; or "All"; optional)

    threshold_templates_access_policy
        Threshold templates access policy (one of:
        "No access", "Allow read-only access to the selected Threshold Template(s)",
        "Allow full access to the selected Threshold Template(s)",
        "Allow full access to the selected Threshold Template(s) and permit adding new ones")

    threshold_templates
        Threshold templates to grant access to (string or list, optional)

    whitelist_templates_access_policy
        Whitelist templates access policy (one of: "No access",
        "Allow read-only access to the selected Whitelist Template(s)",
        "Allow limited access to the selected Whitelist Template(s)"

    whitelist_templates
        Whitelist templates to grant access to (string or list, optional)

    scheduled_reports_access_policy
        Scheduled reports access policy (one of: "No access",
        "Allow full access to the selected Scheduled Report(s) and permit adding new ones")

    scheduled_reports
        Scheduled reports to grant access to (string or list, optional)

    Returns a dict if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    role_data = {}
    if not isinstance(role_name, str):
        log.error('%s Value for "role_name" must be a string.', LOG_PREFIX)
        return False
    else:
        role_data["role_name"] = role_name
    if description is not None:
        if not isinstance(description, str):
            log.error('%s Value for "description" must be a string', LOG_PREFIX)
            return False
        else:
            role_data["description"] = description
    if south_region not in ["Show", "Hide"]:
        log.error('%s Value for "south_region" must be one of: "Show", "Hide".', LOG_PREFIX)
        return False
    else:
        role_data["south_region"] = south_region
    if help_menu not in ["Show", "Hide"]:
        log.error('%s Value for "help_menu" must be one of: "Show", "Hide".', LOG_PREFIX)
        return False
    else:
        role_data["help_menu"] = help_menu
    if comments is not None:
        if not isinstance(comments, str):
            log.error('%s Value for "comments" must be a string', LOG_PREFIX)
            return False
        else:
            role_data["comments"] = comments
    if allow_device_groups is not None:
        if not isinstance(allow_device_groups, str) and allow_device_groups is not list:
            log.error(
                '%s Value for "allow_device_groups" must be a string or a list of strings',
                LOG_PREFIX,
            )
            return False
        elif allow_device_groups is list:
            for item in allow_device_groups:
                if not isinstance(item, str):
                    log.error(
                        '%s Value for "allow_device_groups" must be a string or a list of strings',
                        LOG_PREFIX,
                    )
                    return False
            role_data["allow_device_groups"] = ",".join(allow_device_groups)
        else:
            role_data["allow_device_groups"] = allow_device_groups
    if allow_ip_groups is not None:
        if not isinstance(allow_ip_groups, str) and allow_ip_groups is not list:
            log.error(
                '%s Value for "allow_ip_groups" must be a string or a list of strings', LOG_PREFIX
            )
            return False
        elif allow_ip_groups is list:
            for item in allow_ip_groups:
                if not isinstance(item, str):
                    log.error(
                        '%s Value for "allow_ip_groups" must be a string or a list of strings',
                        LOG_PREFIX,
                    )
                    return False
            role_data["allow_ip_groups"] = ",".join(allow_ip_groups)
        else:
            role_data["allow_ip_groups"] = allow_ip_groups
    if allow_servers is not None:
        if not isinstance(allow_servers, str) and allow_servers is not list:
            log.error(
                '%s Value for "allow_servers" must be a string or a list of strings', LOG_PREFIX
            )
            return False
        elif allow_servers is list:
            for item in allow_servers:
                if not isinstance(item, str):
                    log.error(
                        '%s Value for "allow_servers" must be a string or a list of strings',
                        LOG_PREFIX,
                    )
                    return False
            role_data["allow_servers"] = allow_servers
        else:
            role_data["allow_servers"] = allow_servers.split(",")
        for i in range(len(role_data["allow_servers"])):
            if i != "All":
                server_query = list_servers(server_name=role_data["allow_servers"][i])[0]
                if role_data["allow_servers"][i] == server_query["server_name"]:
                    allow_servers[i] = server_query["server_id"]
                else:
                    log.error(
                        '%s Supplied value "%s" for allow_servers not found',
                        LOG_PREFIX,
                        allow_servers[i],
                    )
                    return False
            elif len(role_data["allow_servers"]) > 1:
                log.error(
                    '%s Supplied value "%s" for allow_servers cannot be used with any other values',
                    LOG_PREFIX,
                    allow_servers[i],
                )
                return False
        role_data["allow_servers"] = ",".join(role_data["allow_servers"])
    if read_only_dashboards is not None:
        if not isinstance(read_only_dashboards, str) and read_only_dashboards is not list:
            log.error(
                '%s Value for "read_only_dashboards" must be a string or a list of strings',
                LOG_PREFIX,
            )
            return False
        elif read_only_dashboards is list:
            for item in read_only_dashboards:
                if not isinstance(item, str):
                    log.error(
                        '%s Value for "read_only_dashboards" must be a string or a list of strings',
                        LOG_PREFIX,
                    )
                    return False
            role_data["read_only_dashboards"] = read_only_dashboards
        else:
            role_data["read_only_dashboards"] = read_only_dashboards.split(",")
        for i in range(len(role_data["read_only_dashboards"])):
            if i != "All":
                read_only_dashboard_query = list_dashboards(
                    dashboard_name=role_data["read_only_dashboards"][i]
                )[0]
                if (
                    role_data["read_only_dashboards"][i]
                    == read_only_dashboard_query["dashboard_name"]
                ):
                    read_only_dashboards[i] = read_only_dashboard_query["dashboard_id"]
                else:
                    log.error(
                        '%s Supplied value "%s" for read_only_dashboards not found',
                        LOG_PREFIX,
                        read_only_dashboards[i],
                    )
                    return False
            elif len(role_data["read_only_dashboards"]) > 1:
                log.error(
                    '%s Supplied value "%s" for read_only_dashboards cannot be used with any other values',
                    LOG_PREFIX,
                    read_only_dashboards[i],
                )
                return False
        role_data["read_only_dashboards"] = ",".join(role_data["read_only_dashboards"])
    if dashboards_access_policy not in [
        "No modification allowed",
        "Allow modifications to Full-access Dashboards",
        "Allow modifications to Full-access Dashboards and permit adding new ones",
    ]:
        log.error(
            '%s Value for "dashboards_access_policy" must be one of: "No modification allowed", '
            '"Allow modifications to Full-access Dashboards", "Allow modifications to Full-access '
            'Dashboards and permit adding new ones".',
            LOG_PREFIX,
        )
        return False
    else:
        role_data["dashboards_access_policy"] = dashboards_access_policy
    if full_dashboards is not None:
        if not isinstance(full_dashboards, str) and full_dashboards is not list:
            log.error(
                '%s Value for "full_dashboards" must be a string or a list of strings',
                LOG_PREFIX,
            )
            return False
        elif full_dashboards is list:
            for item in full_dashboards:
                if not isinstance(item, str):
                    log.error(
                        '%s Value for "full_dashboards" must be a string or a list of strings',
                        LOG_PREFIX,
                    )
                    return False
            role_data["full_dashboards"] = ",".join(full_dashboards)
        else:
            role_data["full_dashboards"] = full_dashboards
        for i in range(len(role_data["full_dashboards"])):
            if i != "All":
                full_dashboard_query = list_dashboards(
                    dashboard_name=role_data["full_dashboards"][i]
                )[0]
                if role_data["full_dashboards"][i] == full_dashboard_query["dashboard_name"]:
                    full_dashboards[i] = full_dashboard_query["dashboard_id"]
                else:
                    log.error(
                        '%s Supplied value "%s" for full_dashboards not found',
                        LOG_PREFIX,
                        full_dashboards[i],
                    )
                    return False
            elif len(role_data["full_dashboards"]) > 1:
                log.error(
                    '%s Supplied value "%s" for full_dashboards cannot be used with any other values',
                    LOG_PREFIX,
                    full_dashboards[i],
                )
                return False
        role_data["full_dashboards"] = ",".join(role_data["full_dashboards"])
    if reports_tools not in ["Show", "Hide"]:
        log.error('%s Value for "reports_tools" must be one of: "Show", "Hide".', LOG_PREFIX)
        return False
    else:
        role_data["reports_tools"] = reports_tools
    if reports_tools_items is not None:
        if not isinstance(reports_tools_items, str) and reports_tools_items is not list:
            log.error(
                '%s Value for "reports_tools_items" must be a string or a list of strings',
                LOG_PREFIX,
            )
            return False
        elif reports_tools_items is list:
            for item in reports_tools_items:
                if item not in [
                    "Anomalies",
                    "Anomalies Actions",
                    "Routing",
                    "Routing Actions",
                    "Firewall",
                    "Firewall Actions",
                    "Flows",
                    "Packets",
                    "Packets Actions & Captures",
                    "Packets Actions",
                ]:
                    log.error(
                        '%s Value for "reports_tools_items" must be a string or a list of strings '
                        'with one or more of the following: "Anomalies", "Anomalies Actions", '
                        '"Routing", "Routing Actions", "Firewall", "Firewall Actions", "Flows", '
                        '"Packets", "Packets Actions & Captures", "Packets Actions"',
                        LOG_PREFIX,
                    )
                    return False
            role_data["read_only_dashboards"] = read_only_dashboards
        elif reports_tools_items not in [
            "Anomalies",
            "Anomalies Actions",
            "Routing",
            "Routing Actions",
            "Firewall",
            "Firewall Actions",
            "Flows",
            "Packets",
            "Packets Actions & Captures",
            "Packets Actions",
        ]:
            log.error(
                '%s Value for "reports_tools_items" must be a string or a list of strings with one '
                'or more of the following: "Anomalies", "Anomalies Actions", "Routing", "Routing '
                'Actions", "Firewall", "Firewall Actions", "Flows", "Packets", "Packets Actions '
                '& Captures", "Packets Actions"',
                LOG_PREFIX,
            )
            return False
        else:
            role_data["reports_tools_items"] = reports_tools_items.split(",")
    if reports_devices not in ["Show", "Hide"]:
        log.error('%s Value for "reports_devices" must be one of: "Show", "Hide".', LOG_PREFIX)
        return False
    else:
        role_data["reports_devices"] = reports_devices
    if reports_dashboards not in ["Show", "Hide"]:
        log.error('%s Value for "reports_dashboards" must be one of: "Show", "Hide".', LOG_PREFIX)
        return False
    else:
        role_data["reports_dashboards"] = reports_dashboards
    if reports_ip_addresses not in ["Show", "Hide"]:
        log.error('%s Value for "reports_ip_addresses" must be one of: "Show", "Hide".', LOG_PREFIX)
        return False
    else:
        role_data["reports_ip_addresses"] = reports_ip_addresses
    if reports_ip_groups not in ["Show", "Hide"]:
        log.error('%s Value for "reports_ip_groups" must be one of: "Show", "Hide".', LOG_PREFIX)
        return False
    else:
        role_data["reports_ip_groups"] = reports_ip_groups
    if reports_servers not in ["Show", "Hide"]:
        log.error('%s Value for "reports_servers" must be one of: "Show", "Hide".', LOG_PREFIX)
        return False
    else:
        role_data["reports_servers"] = reports_servers
    if ipzones_access_policy not in [
        "No access",
        "Allow full access to the allowed IP Group(s) in the selected IP Zone(s)",
    ]:
        log.error(
            '%s Value for "ipzones_access_policy" must be one of: "No access", "Allow full access '
            'to the allowed IP Group(s) in the selected IP Zone(s)".',
            LOG_PREFIX,
        )
        return False
    else:
        role_data["ipzones_access_policy"] = ipzones_access_policy
    if ipzones is not None:
        if not isinstance(ipzones, str) and ipzones is not list:
            log.error(
                '%s Value for "ipzones" must be a string or a list of strings',
                LOG_PREFIX,
            )
            return False
        elif ipzones is list:
            for item in ipzones:
                if not isinstance(item, str):
                    log.error(
                        '%s Value for "ipzones" must be a string or a list of strings',
                        LOG_PREFIX,
                    )
                    return False
            role_data["ipzones"] = ",".join(ipzones)
        else:
            role_data["ipzones"] = ipzones
        for i in range(len(role_data["ipzones"])):
            if i != "All":
                ipzone_query = list_ip_zones(ip_zone_name=role_data["ipzones"][i])[0]
                if role_data["ipzones"][i] == ipzone_query["ip_zone_name"]:
                    ipzones[i] = ipzone_query["ipzone_id"]
                else:
                    log.error(
                        '%s Supplied value "%s" for ipzones not found',
                        LOG_PREFIX,
                        ipzones[i],
                    )
                    return False
            elif len(role_data["ipzones"]) > 1:
                log.error(
                    '%s Supplied value "%s" for ipzones cannot be used with any other values',
                    LOG_PREFIX,
                    ipzones[i],
                )
                return False
        role_data["ipzones"] = ",".join(role_data["ipzones"])
    if responses is not None:
        if not isinstance(responses, str) and responses is not list:
            log.error(
                '%s Value for "responses" must be a string or a list of strings',
                LOG_PREFIX,
            )
            return False
        elif responses is list:
            for item in responses:
                if not isinstance(item, str):
                    log.error(
                        '%s Value for "responses" must be a string or a list of strings',
                        LOG_PREFIX,
                    )
                    return False
            role_data["responses"] = ",".join(responses)
        else:
            role_data["responses"] = responses
        for i in range(len(role_data["responses"])):
            if i != "All":
                response_query = list_responses(response_name=role_data["responses"][i])[0]
                if role_data["responses"][i] == response_query["response_name"]:
                    responses[i] = response_query["response_id"]
                else:
                    log.error(
                        '%s Supplied value "%s" for responses not found',
                        LOG_PREFIX,
                        responses[i],
                    )
                    return False
            elif len(role_data["responses"]) > 1:
                log.error(
                    '%s Supplied value "%s" for responses cannot be used with any other values',
                    LOG_PREFIX,
                    responses[i],
                )
                return False
        role_data["responses"] = ",".join(role_data["responses"])
    if response_actions is not None:
        if not isinstance(response_actions, str) and response_actions is not list:
            log.error(
                '%s Value for "response_actions" must be a string or a list of strings',
                LOG_PREFIX,
            )
            return False
        elif response_actions is list:
            for item in response_actions:
                if item not in [
                    "Announce the destination IP address in a BGP routing update",
                    "Capture a sample of malicious packets",
                    "Detect filtering rules and mitigate the attack with Wanguard Filter",
                    "Execute a command or script with dynamic parameters as arguments",
                    "Send a custom email notification",
                    "Send a custom Syslog message",
                    "Send a custom SNMP trap",
                    "Send a visual or audio notification to all logged-in Console users",
                    "Generate an anomaly report and send it by email",
                    "Announce a BGP routing update with Flowspec or S/RTBH",
                    "Apply the filtering rule on a third-party inline device",
                    "Capture a sample of packets matched by the filtering rule",
                    "All",
                ]:
                    log.error(
                        '%s Value for "response_actions" must be a string or a list of strings '
                        "with one or more of the following: "
                        '"Announce the destination IP address in a BGP routing update", '
                        '"Capture a sample of malicious packets", '
                        '"Detect filtering rules and mitigate the attack with Wanguard Filter", '
                        '"Execute a command or script with dynamic parameters as arguments", '
                        '"Send a custom email notification", "Send a custom Syslog message", '
                        '"Send a custom SNMP trap", '
                        '"Send a visual or audio notification to all logged-in Console users", '
                        '"Generate an anomaly report and send it by email", '
                        '"Announce a BGP routing update with Flowspec or S/RTBH", '
                        '"Apply the filtering rule on a third-party inline device", '
                        '"Capture a sample of packets matched by the filtering rule", "All"',
                        LOG_PREFIX,
                    )
                    return False
            role_data["read_only_dashboards"] = read_only_dashboards
        elif response_actions not in [
            "Announce the destination IP address in a BGP routing update",
            "Capture a sample of malicious packets",
            "Detect filtering rules and mitigate the attack with Wanguard Filter",
            "Execute a command or script with dynamic parameters as arguments",
            "Send a custom email notification",
            "Send a custom Syslog message",
            "Send a custom SNMP trap",
            "Send a visual or audio notification to all logged-in Console users",
            "Generate an anomaly report and send it by email",
            "Announce a BGP routing update with Flowspec or S/RTBH",
        ]:
            log.error(
                '%s Value for "response_actions" must be a string or a list of strings with one or '
                '"more of the following: "'
                '"Announce the destination IP address in a BGP routing update", '
                '"Capture a sample of malicious packets", '
                '"Detect filtering rules and mitigate the attack with Wanguard Filter", '
                '"Execute a command or script with dynamic parameters as arguments", '
                '"Send a custom email notification", "Send a custom Syslog message", '
                '"Send a custom SNMP trap", '
                '"Send a visual or audio notification to all logged-in Console users", '
                '"Generate an anomaly report and send it by email", '
                '"Announce a BGP routing update with Flowspec or S/RTBH", '
                '"Apply the filtering rule on a third-party inline device", '
                '"Capture a sample of packets matched by the filtering rule", "All"',
                LOG_PREFIX,
            )
            return False
        else:
            role_data["response_actions"] = response_actions.split(",")
    if threshold_templates_access_policy not in [
        "No access",
        "Allow read-only access to the selected Threshold Template(s)",
        "Allow full access to the selected Threshold Template(s)",
        "Allow full access to the selected Threshold Template(s) and permit adding new ones",
    ]:
        log.error(
            '%s Value for "threshold_templates_access_policy" must be one of: "No access", '
            '"Allow read-only access to the selected Threshold Template(s)", '
            '"Allow full access to the selected Threshold Template(s)",'
            '"Allow full access to the selected Threshold Template(s) and permit adding new ones"',
            LOG_PREFIX,
        )
        return False
    else:
        role_data["threshold_templates_access_policy"] = threshold_templates_access_policy
    if threshold_templates is not None:
        if not isinstance(threshold_templates, str) and threshold_templates is not list:
            log.error(
                '%s Value for "threshold_templates" must be a string or a list of strings',
                LOG_PREFIX,
            )
            return False
        elif threshold_templates is list:
            for item in threshold_templates:
                if not isinstance(item, str):
                    log.error(
                        '%s Value for "threshold_templates" must be a string or a list of strings',
                        LOG_PREFIX,
                    )
                    return False
            role_data["threshold_templates"] = ",".join(threshold_templates)
        else:
            role_data["threshold_templates"] = threshold_templates
        for i in range(len(role_data["threshold_templates"])):
            if i != "All":
                threshold_template_query = list_threshold_templates(
                    threshold_template_name=role_data["threshold_templates"][i]
                )[0]
                if (
                    role_data["threshold_templates"][i]
                    == threshold_template_query["threshold_template_name"]
                ):
                    threshold_templates[i] = threshold_template_query["threshold_template_id"]
                else:
                    log.error(
                        '%s Supplied value "%s" for threshold_templates not found',
                        LOG_PREFIX,
                        threshold_templates[i],
                    )
                    return False
            elif len(role_data["threshold_templates"]) > 1:
                log.error(
                    '%s Supplied value "%s" for threshold_templates cannot be used with any other values',
                    LOG_PREFIX,
                    threshold_templates[i],
                )
                return False
        role_data["threshold_templates"] = ",".join(role_data["threshold_templates"])
    if whitelist_templates_access_policy not in [
        "No access",
        "Allow read-only access to the selected Whitelist Template(s)",
        "Allow limited access to the selected Whitelist Template(s)",
    ]:
        log.error(
            '%s Value for "whitelist_templates_access_policy" must be one of: "No access", '
            '"Allow read-only access to the selected Whitelist Template(s)", '
            '"Allow limited access to the selected Whitelist Template(s)"',
            LOG_PREFIX,
        )
        return False
    else:
        role_data["whitelist_templates_access_policy"] = whitelist_templates_access_policy
    if whitelist_templates is not None:
        if not isinstance(whitelist_templates, str) and whitelist_templates is not list:
            log.error(
                '%s Value for "whitelist_templates" must be a string or a list of strings',
                LOG_PREFIX,
            )
            return False
        elif whitelist_templates is list:
            for item in whitelist_templates:
                if not isinstance(item, str):
                    log.error(
                        '%s Value for "whitelist_templates" must be a string or a list of strings',
                        LOG_PREFIX,
                    )
                    return False
            role_data["whitelist_templates"] = ",".join(whitelist_templates)
        else:
            role_data["whitelist_templates"] = whitelist_templates
        for i in range(len(role_data["whitelist_templates"])):
            if i != "All":
                whitelist_template_query = list_whitelist_templates(
                    whitelist_template_name=role_data["whitelist_templates"][i]
                )[0]
                if (
                    role_data["whitelist_templates"][i]
                    == whitelist_template_query["whitelist_template_name"]
                ):
                    whitelist_templates[i] = whitelist_template_query["whitelist_template_id"]
                else:
                    log.error(
                        '%s Supplied value "%s" for whitelist_templates not found',
                        LOG_PREFIX,
                        whitelist_templates[i],
                    )
                    return False
            elif len(role_data["whitelist_templates"]) > 1:
                log.error(
                    '%s Supplied value "%s" for whitelist_templates cannot be used with any other values',
                    LOG_PREFIX,
                    whitelist_templates[i],
                )
                return False
        role_data["whitelist_templates"] = ",".join(role_data["whitelist_templates"])

    if scheduled_reports_access_policy not in [
        "No access",
        "Allow full access to the selected Scheduled Report(s) and permit adding new ones",
    ]:
        log.error(
            '%s Value for "scheduled_reports_access_policy" must be one of: "No access", '
            '"Allow full access to the selected Scheduled Report(s) and permit adding new ones"',
            LOG_PREFIX,
        )
        return False
    else:
        role_data["scheduled_reports_access_policy"] = scheduled_reports_access_policy
    if scheduled_reports is not None:
        if not isinstance(scheduled_reports, str) and scheduled_reports is not list:
            log.error(
                '%s Value for "scheduled_reports" must be a string or a list of strings',
                LOG_PREFIX,
            )
            return False
        elif scheduled_reports is list:
            for item in scheduled_reports:
                if not isinstance(item, str):
                    log.error(
                        '%s Value for "scheduled_reports" must be a string or a list of strings',
                        LOG_PREFIX,
                    )
                    return False
            role_data["scheduled_reports"] = ",".join(scheduled_reports)
        else:
            role_data["scheduled_reports"] = scheduled_reports
    headers = _get_headers()
    api_url = config["api_url"]
    role_url = f"{api_url}/v1/roles"
    role_results = {}
    role_ret = salt.utils.http.query(
        role_url,
        method="POST",
        data=json.dumps(role_data),
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in role_ret:
        log.error(
            '%s API query failed for "get_role", status code: %s, error %s',
            LOG_PREFIX,
            role_ret["status"],
            role_ret["error"],
        )
        return False
    else:
        role_results = json.loads(role_ret["body"])
        if role_results.get("success"):
            return True

    return False


def list_servers(opts=None, server_name=None):
    """
    List all servers

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    server_name
        Search after server name (string, optional)

    Returns a list of dicts if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    params = {}
    if server_name is not None:
        if not isinstance(server_name, str):
            log.error('%s Value for "server_name" must be a string.', LOG_PREFIX)
            return False
        else:
            params["server_name"] = server_name
    if params:
        params = urllib.parse.urlencode(params)
        params = "?" + params
    else:
        params = ""
    headers = _get_headers()
    api_url = config["api_url"]
    servers_url = f"{api_url}/v1/servers{params}"
    servers_results = []
    servers_ret = salt.utils.http.query(
        servers_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in servers_ret:
        log.error(
            '%s API query failed for "list_servers", status code: %s, error %s',
            LOG_PREFIX,
            servers_ret["status"],
            servers_ret["error"],
        )
        return False
    else:
        servers_results = servers_ret["dict"]
        for i, server in enumerate(servers_results):
            del servers_results[i]["href"]
        return servers_results

    return False


def get_server(opts=None, server_name=None, server_id=None):
    """
    Get the configuration of a server

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    server_name
        Server name (string)

    server_id
        Server ID (integer)

    If both server_name and server_id are supplied, server_name will be used.

    Returns a dict if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    if server_name is not None:
        if not isinstance(server_name, str):
            log.error('%s Value for "server_name" must be a string.', LOG_PREFIX)
            return False
        server_list = list_servers(opts=opts, server_name=server_name)
        if not server_list:
            log.error('%s Server "%s" not found.', LOG_PREFIX, server_name)
            return False
        for i, server in enumerate(server_list):
            if server["server_name"] == server_name:
                server_id = int(server["server_id"])
                break
            if i == len(server_list) - 1:
                log.error('%s Server "%s" not found.', LOG_PREFIX, server_name)
                return False
    elif not isinstance(server_id, int):
        log.error('%s Value for "server_id" must be a postive integer.', LOG_PREFIX)
        return False
    headers = _get_headers()
    api_url = config["api_url"]
    server_url = f"{api_url}/v1/servers/{server_id}"
    server_results = {}
    server_ret = salt.utils.http.query(
        server_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in server_ret:
        log.error(
            '%s API query failed for "get_server", status code: %s, error %s',
            LOG_PREFIX,
            server_ret["status"],
            server_ret["error"],
        )
        return False
    else:
        server_results = server_ret["dict"]
        return server_results

    return False


def list_dashboards(opts=None, dashboard_name=None):
    """
    List all dashboards

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    dashboard_name
        Search after dashboard name (string, optional)

    Returns a list of dicts if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    params = {}
    if dashboard_name is not None:
        if not isinstance(dashboard_name, str):
            log.error('%s Value for "dashboard_name" must be a string.', LOG_PREFIX)
            return False
        else:
            params["dashboard_name"] = dashboard_name
    if params:
        params = urllib.parse.urlencode(params)
        params = "?" + params
    else:
        params = ""
    headers = _get_headers()
    api_url = config["api_url"]
    dashboards_url = f"{api_url}/v1/dashboards{params}"
    dashboards_results = []
    dashboards_ret = salt.utils.http.query(
        dashboards_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in dashboards_ret:
        log.error(
            '%s API query failed for "list_dashboards", status code: %s, error %s',
            LOG_PREFIX,
            dashboards_ret["status"],
            dashboards_ret["error"],
        )
        return False
    else:
        dashboards_results = dashboards_ret["dict"]
        return dashboards_results

    return False


def list_ip_zones(opts=None, ip_zone_name=None):
    """
    List all IP zones

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    ip_zone_name
        Search after ip_zone name (string, optional)

    Returns a list of dicts if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    params = {}
    if ip_zone_name is not None:
        if not isinstance(ip_zone_name, str):
            log.error('%s Value for "ip_zone_name" must be a string.', LOG_PREFIX)
            return False
        else:
            params["ip_zone_name"] = ip_zone_name
    if params:
        params = urllib.parse.urlencode(params)
        params = "?" + params
    else:
        params = ""
    headers = _get_headers()
    api_url = config["api_url"]
    ip_zones_url = f"{api_url}/v1/ip_zones{params}"
    ip_zones_results = []
    ip_zones_ret = salt.utils.http.query(
        ip_zones_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in ip_zones_ret:
        log.error(
            '%s API query failed for "list_ip_zones", status code: %s, error %s',
            LOG_PREFIX,
            ip_zones_ret["status"],
            ip_zones_ret["error"],
        )
        return False
    else:
        ip_zones_results = ip_zones_ret["dict"]
        for i, ip_zone in enumerate(ip_zones_results):
            del ip_zones_results[i]["href"]
        return ip_zones_results

    return False


def list_responses(opts=None, response_name=None):
    """
    List all responses

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    response_name
        Search after response name (string, optional)

    Returns a list of dicts if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    params = {}
    if response_name is not None:
        if not isinstance(response_name, str):
            log.error('%s Value for "response_name" must be a string.', LOG_PREFIX)
            return False
        else:
            params["response_name"] = response_name
    if params:
        params = urllib.parse.urlencode(params)
        params = "?" + params
    else:
        params = ""
    headers = _get_headers()
    api_url = config["api_url"]
    responses_url = f"{api_url}/v1/responses{params}"
    responses_results = []
    responses_ret = salt.utils.http.query(
        responses_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in responses_ret:
        log.error(
            '%s API query failed for "list_responses", status code: %s, error %s',
            LOG_PREFIX,
            responses_ret["status"],
            responses_ret["error"],
        )
        return False
    else:
        responses_results = responses_ret["dict"]
        for i, response in enumerate(responses_results):
            del responses_results[i]["href"]
        return responses_results

    return False


def get_response(opts=None, response_name=None, response_id=None):
    """
    Get the configuration of a response

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    response_name
        Response name (string)

    response_id
        Response ID (integer)

    If both response_name and response_id are supplied, response_name will be used.

    Returns a dict if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    if response_name is not None:
        if not isinstance(response_name, str):
            log.error('%s Value for "response_name" must be a string.', LOG_PREFIX)
            return False
        response_list = list_responses(opts=opts, response_name=response_name)
        if not response_list:
            log.error('%s Response "%s" not found.', LOG_PREFIX, response_name)
            return False
        for i, response in enumerate(response_list):
            if response["response_name"] == response_name:
                response_id = int(response["response_id"])
                break
            if i == len(response_list) - 1:
                log.error('%s Response "%s" not found.', LOG_PREFIX, response_name)
                return False
    elif not isinstance(response_id, int):
        log.error('%s Value for "response_id" must be a postive integer.', LOG_PREFIX)
        return False
    headers = _get_headers()
    api_url = config["api_url"]
    response_url = f"{api_url}/v1/responses/{response_id}"
    response_results = {}
    response_ret = salt.utils.http.query(
        response_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in response_ret:
        log.error(
            '%s API query failed for "get_response", status code: %s, error %s',
            LOG_PREFIX,
            response_ret["status"],
            response_ret["error"],
        )
        return False
    else:
        response_results = response_ret["dict"]
        return response_results

    return False


def list_threshold_templates(opts=None, threshold_template_name=None):
    """
    List all threshold templates

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    threshold_template_name
        Search after threshold_template name (string, optional)

    Returns a list of dicts if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    params = {}
    if threshold_template_name is not None:
        if not isinstance(threshold_template_name, str):
            log.error('%s Value for "threshold_template_name" must be a string.', LOG_PREFIX)
            return False
        else:
            params["threshold_template_name"] = threshold_template_name
    if params:
        params = urllib.parse.urlencode(params)
        params = "?" + params
    else:
        params = ""
    headers = _get_headers()
    api_url = config["api_url"]
    threshold_templates_url = f"{api_url}/v1/threshold_templates{params}"
    threshold_templates_results = []
    threshold_templates_ret = salt.utils.http.query(
        threshold_templates_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in threshold_templates_ret:
        log.error(
            '%s API query failed for "list_threshold_templates", status code: %s, error %s',
            LOG_PREFIX,
            threshold_templates_ret["status"],
            threshold_templates_ret["error"],
        )
        return False
    else:
        threshold_templates_results = threshold_templates_ret["dict"]
        for i, threshold_template in enumerate(threshold_templates_results):
            del threshold_templates_results[i]["href"]
        return threshold_templates_results

    return False


def get_threshold_template(opts=None, threshold_template_name=None, threshold_template_id=None):
    """
    Get the configuration of a threshold template

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    threshold_template_name
        Threshold template name (string)

    threshold_template_id
        Threshold template ID (integer)

    If both threshold_template_name and threshold_template_id are supplied, threshold_template_name will be used.

    Returns a dict if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    if threshold_template_name is not None:
        if not isinstance(threshold_template_name, str):
            log.error('%s Value for "threshold_template_name" must be a string.', LOG_PREFIX)
            return False
        threshold_template_list = list_threshold_templates(
            opts=opts, threshold_template_name=threshold_template_name
        )
        if not threshold_template_list:
            log.error('%s Threshold template "%s" not found.', LOG_PREFIX, threshold_template_name)
            return False
        for i, threshold_template in enumerate(threshold_template_list):
            if threshold_template["threshold_template_name"] == threshold_template_name:
                threshold_template_id = int(threshold_template["threshold_template_id"])
                break
            if i == len(threshold_template_list) - 1:
                log.error(
                    '%s Threshold template "%s" not found.', LOG_PREFIX, threshold_template_name
                )
                return False
    elif not isinstance(threshold_template_id, int):
        log.error('%s Value for "threshold_template_id" must be a postive integer.', LOG_PREFIX)
        return False
    headers = _get_headers()
    api_url = config["api_url"]
    threshold_template_url = f"{api_url}/v1/threshold_templates/{threshold_template_id}"
    threshold_template_results = {}
    threshold_template_ret = salt.utils.http.query(
        threshold_template_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in threshold_template_ret:
        log.error(
            '%s API query failed for "get_threshold_template", status code: %s, error %s',
            LOG_PREFIX,
            threshold_template_ret["status"],
            threshold_template_ret["error"],
        )
        return False
    else:
        threshold_template_results = threshold_template_ret["dict"]
        return threshold_template_results

    return False


def list_whitelist_templates(opts=None, whitelist_template_name=None):
    """
    List all whitelist templates

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    whitelist_template_name
        Search after whitelist_template name (string, optional)

    Returns a list of dicts if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    params = {}
    if whitelist_template_name is not None:
        if not isinstance(whitelist_template_name, str):
            log.error('%s Value for "whitelist_template_name" must be a string.', LOG_PREFIX)
            return False
        else:
            params["whitelist_template_name"] = whitelist_template_name
    if params:
        params = urllib.parse.urlencode(params)
        params = "?" + params
    else:
        params = ""
    headers = _get_headers()
    api_url = config["api_url"]
    whitelist_templates_url = f"{api_url}/v1/whitelist_templates{params}"
    whitelist_templates_results = []
    whitelist_templates_ret = salt.utils.http.query(
        whitelist_templates_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=config["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in whitelist_templates_ret:
        log.error(
            '%s API query failed for "list_whitelist_templates", status code: %s, error %s',
            LOG_PREFIX,
            whitelist_templates_ret["status"],
            whitelist_templates_ret["error"],
        )
        return False
    else:
        whitelist_templates_results = whitelist_templates_ret["dict"]
        for i, whitelist_template in enumerate(whitelist_templates_results):
            del whitelist_templates_results[i]["href"]
        return whitelist_templates_results

    return False


def get_whitelist_template(opts=None, whitelist_template_name=None, whitelist_template_id=None):
    """
    Get the configuration of a whitelist template

    opts
        Dictionary containing the following keys:

        api_url: ``http://localhost/wanguard-api``
            The URL for the Wanguard REST API

        username: ``None``
            The username to access the API. This user must have API access enabled

        password: ``None``
            The password to access the API.

    whitelist_template_name
        Whitelist template name (string)

    whitelist_template_id
        Whitelist template ID (integer)

    If both whitelist_template_name and whitelist_template_id are supplied, whitelist_template_name will be used.

    Returns a dict if successful or ``False`` if unsuccessful.
    """
    config = _validate_opts(opts=opts, opts_list=["api_url", "username", "password"])
    if not config:
        log.error("%s Invalid configuration supplied", LOG_PREFIX)
        return {"Error": "Invalid configuration supplied"}
    elif config.get("Error"):
        log.error("%s %s", LOG_PREFIX, config["Error"])
        return {"Error": config["Error"]}
    if whitelist_template_name is not None:
        if not isinstance(whitelist_template_name, str):
            log.error('%s Value for "whitelist_template_name" must be a string.', LOG_PREFIX)
            return False
        whitelist_template_list = list_whitelist_templates(
            opts=opts, whitelist_template_name=whitelist_template_name
        )
        if not whitelist_template_list:
            log.error('%s Whitelist template "%s" not found.', LOG_PREFIX, whitelist_template_name)
            return False
        for i, whitelist_template in enumerate(whitelist_template_list):
            if whitelist_template["whitelist_template_name"] == whitelist_template_name:
                whitelist_template_id = int(whitelist_template["whitelist_template_id"])
                break
            if i == len(whitelist_template_list) - 1:
                log.error(
                    '%s Whitelist template "%s" not found.', LOG_PREFIX, whitelist_template_name
                )
                return False
    elif not isinstance(whitelist_template_id, int):
        log.error('%s Value for "whitelist_template_id" must be a postive integer.', LOG_PREFIX)
        return False
    headers = _get_headers()
    api_url = config["api_url"]
    whitelist_template_url = f"{api_url}/v1/whitelist_templates/{whitelist_template_id}"
    whitelist_template_results = {}
    whitelist_template_ret = salt.utils.http.query(
        whitelist_template_url,
        method="GET",
        header_dict=headers,
        username=config["username"],
        password=opts["password"],
        decode=True,
    )
    # Check status code for API call
    if "error" in whitelist_template_ret:
        log.error(
            '%s API query failed for "get_whitelist_template", status code: %s, error %s',
            LOG_PREFIX,
            whitelist_template_ret["status"],
            whitelist_template_ret["error"],
        )
        return False
    else:
        whitelist_template_results = whitelist_template_ret["dict"]
        return whitelist_template_results

    return False

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""
Wanguard execution module

This module allows management of Andrisoft Wanguard software

This module relies on the `Wanguard REST API <https://www.andrisoft.com/wanguard-api-ui/`_. You must
ensure you have the `wanrestapi` package installed on your Wanguard Console server.

This module also requires a configuration profile to be configured in either the
minion or master configuration file.

Configuration example:

.. code-block:: yaml

    my-wanguard-server:
      driver: wanguard
      api_url: http://localhost/wanguard-api
      username: admin
      password: CorrectHorseBatteryStaple

The ``driver`` refers to the Wanguard module, and must be set to ``wanguard``
in order to use this module.

Other configuration options:

api_url: ``http://localhost/wanguard-api``
    The URL for the Wanguard REST API.

username: ``None``
    The username to access the API. This user must have API access enabled

password: ``None``
    The password to access the API.

"""

import logging

import saltext.wanguard.utils.wanguard_mod as wanguard_util

log = logging.getLogger(__name__)

# Prefix that is appended to all log entries
LOG_PREFIX = "wanguard:"

__virtualname__ = "wanguard"


def __virtual__():
    # To force a module not to load return something like:
    #   return (False, "The wanguard execution module is not implemented yet")
    return __virtualname__


def _get_config(profile=None):  # pylint: disable=C0116
    config = {}
    if profile:
        config = __salt__["config.get"](profile)
        if config.get("driver") != "wanguard":
            log.error(
                '%s The specified profile "%s" is not a wanguard profile', LOG_PREFIX, profile
            )
            return {}
    else:
        config = __salt__["config.get"]("wanguard")
    if not config:
        log.debug("%s The config is not set", LOG_PREFIX)
        return {}

    return config


def get_role(role_name=None, role_id=None, profile=None):
    """
    Get the configuration of a role

    role_name
        Role name (string)

    role_id
        Role ID (integer)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.get_role role_name=Console

    If both role_name and role_id are supplied, role_name will be used.

    Returns a dictionary containing the role if successful or ``False`` if unsuccessful
    """
    opts = _get_config(profile=profile)
    return wanguard_util.get_role(opts=opts, role_name=role_name, role_id=role_id)


def list_roles(role_name=None, profile=None):
    """
    List all roles

    role_name
        Search after role name (string, optional)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.list_roles role_name="Example Role"

    Returns a list of dictionaries if successful or ``False`` if unsuccessful.
    """
    opts = _get_config(profile=profile)
    return wanguard_util.list_roles(opts=opts, role_name=role_name)


def get_server(server_name=None, server_id=None, profile=None):
    """
    Get the configuration of a server

    server_name
        Server name (string)

    server_id
        Server ID (integer)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.get_server server_name=Console

    If both server_name and server_id are supplied, server_name will be used.

    Returns a dictionary containing the server if successful or ``False`` if unsuccessful
    """
    opts = _get_config(profile=profile)
    return wanguard_util.get_server(opts=opts, server_name=server_name, server_id=server_id)


def list_servers(server_name=None, profile=None):
    """
    List all servers

    server_name
        Search after server name (string, optional)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.list_servers server_name="Example Server"

    Returns a list of dictionaries if successful or ``False`` if unsuccessful.
    """
    opts = _get_config(profile=profile)
    return wanguard_util.list_servers(opts=opts, server_name=server_name)


def list_dashboards(dashboard_name=None, profile=None):
    """
    List all dashboards

    dashboard_name
        Search after dashboard name (string, optional)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.list_dashboards dashboard_name="Example Dashboard"

    Returns a list of dictionaries if successful or ``False`` if unsuccessful.
    """
    opts = _get_config(profile=profile)
    return wanguard_util.list_dashboards(opts=opts, dashboard_name=dashboard_name)


def list_ip_zones(ip_zone_name=None, profile=None):
    """
    List all IP zones

    ip_zone_name
        Search after IP zone name (string, optional)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.list_ip_zones  ip_zone_name="Example IP Zone"

    Returns a list of dictionaries if successful or ``False`` if unsuccessful.
    """
    opts = _get_config(profile=profile)
    return wanguard_util.list_ip_zones(opts=opts, ip_zone_name=ip_zone_name)


def get_response(response_name=None, response_id=None, profile=None):
    """
    Get the configuration of a response

    response_name
        Response name (string)

    response_id
        Response ID (integer)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.get_response response_id=7

    If both response_name and response_id are supplied, response_name will be used.

    Returns a dictionary containing the response if successful or ``False`` if unsuccessful
    """
    opts = _get_config(profile=profile)
    return wanguard_util.get_response(
        opts=opts, response_name=response_name, response_id=response_id
    )


def list_responses(response_name=None, profile=None):
    """
    List all responses

    response_name
        Search after response name (string, optional)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.list_responses response_name="Example Response"

    Returns a list of dictionaries if successful or ``False`` if unsuccessful.
    """
    opts = _get_config(profile=profile)
    return wanguard_util.list_responses(opts=opts, response_name=response_name)


def get_threshold_template(threshold_template_name=None, threshold_template_id=None, profile=None):
    """
    Get the configuration of a threshold template

    threshold_template_name
        Threshold template name (string)

    threshold_template_id
        Threshold template ID (integer)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.get_threshold_template threshold_name="Example Threshold Template"

    Returns a dictionary containing the threshold_template if successful or ``False`` if unsuccessful
    """
    opts = _get_config(profile=profile)
    return wanguard_util.get_threshold_template(
        opts=opts,
        threshold_template_name=threshold_template_name,
        threshold_template_id=threshold_template_id,
    )


def list_threshold_templates(threshold_template_name=None, profile=None):
    """
    List all threshold templates

    threshold_template_name
        Search after threshold_template name (string, optional)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.list_threshold_templates threshold_template_name="Example Threshold Template"

    Returns a list of dictionaries if successful or ``False`` if unsuccessful.
    """
    opts = _get_config(profile=profile)
    return wanguard_util.list_threshold_templates(
        opts=opts, threshold_template_name=threshold_template_name
    )


def get_whitelist_template(whitelist_template_name=None, whitelist_template_id=None, profile=None):
    """
    Get the configuration of a whitelist template

    whitelist_template_name
        Whitelist template name (string)

    whitelist_template_id
        Whitelist template ID (integer)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.get_whitelist_template whitelist_template_id=3

    Returns a dictionary containing the whitelist_template if successful or ``False`` if unsuccessful
    """
    opts = _get_config(profile=profile)
    return wanguard_util.get_whitelist_template(
        opts=opts,
        whitelist_template_name=whitelist_template_name,
        whitelist_template_id=whitelist_template_id,
    )


def list_whitelist_templates(whitelist_template_name=None, profile=None):
    """
    List all whitelist templates

    whitelist_template_name
        Search after whitelist_template name (string, optional)

    profile
        Profile to use (optional)

    CLI Example:

    .. code-block:: bash

        salt '*' wanguard.list_whitelist_templates whitelist_template_name="Example Whitelist Template"

    Returns a list of dictionaries if successful or ``False`` if unsuccessful.
    """
    opts = _get_config(profile=profile)
    return wanguard_util.list_whitelist_templates(
        opts=opts, whitelist_template_name=whitelist_template_name
    )

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""
Wanguard state module

This module allows management of Andrisoft Wanguard software

This module relies on the `Wanguard REST API <https://www.andrisoft.com/wanguard-api-ui/`_. You must
ensure you have the `wanrestapi` package installed on your Wanguard Console server.

This module also requires a configuration profile to be configured in either the
minion or master configuration file.

Configuration example:

.. code-block:: yaml

    my-wanguard-server:
      driver: wanguard
      api_url: http://localhost/wanguard-api
      username: admin
      password: CorrectHorseBatteryStaple

The ``driver`` refers to the Wanguard module, and must be set to ``wanguard``
in order to use this module.

Other configuration options:

api_url: ``http://localhost/wanguard-api``
    The URL for the Wanguard REST API.

username: ``None``
    The username to access the API. This user must have API access enabled

password: ``None``
    The password to access the API.

"""

import logging

import dictdiffer

import saltext.wanguard.utils.wanguard_mod as wanguard_util

log = logging.getLogger(__name__)

# Prefix that is appended to all log entries
LOG_PREFIX = "wanguard:"

__virtualname__ = "wanguard"


def __virtual__():
    # To force a module not to load return something like:
    #   return (False, "The wanguard state module is not implemented yet")
    return __virtualname__


def _get_config(profile=None):  # pylint: disable=C0116
    config = {}
    if profile:
        config = __salt__["config.get"](profile)
        if config.get("driver") != "wanguard":
            log.error(
                '%s The specified profile "%s" is not a wanguard profile', LOG_PREFIX, profile
            )
            return {}
    else:
        config = __salt__["config.get"]("wanguard")
    if not config:
        log.debug("%s The config is not set", LOG_PREFIX)
        return {}

    return config


def role_present(
    name=None,
    description=None,
    south_region=None,
    help_menu=None,
    comments=None,
    allow_device_groups=None,
    allow_ip_groups=None,
    allow_servers=None,
    read_only_dashboards=None,
    dashboards_access_policy=None,
    full_dashboards=None,
    reports_tools=None,
    reports_tools_items=None,
    reports_devices=None,
    reports_dashboards=None,
    reports_ip_addresses=None,
    reports_ip_groups=None,
    reports_servers=None,
    ipzones_access_policy=None,
    ipzones=None,
    responses_access_policy=None,
    responses=None,
    response_actions=None,
    threshold_templates_access_policy=None,
    threshold_templates=None,
    whitelist_templates_access_policy=None,
    whitelist_templates=None,
    scheduled_reports_access_policy=None,
    scheduled_reports=None,
    profile=None,
):
    """
    Ensure the named role is present in Wanguard

    name
        Name of the role (string)

    description
        Description of the role (string, optional)

    south_region
        Display the South Region in the Wanguard console (one of: "Show", "Hide")

    help_menu
        Display the Help menu in the Wanguard console (one of: "Show", "Hide")

    comments
        Comments associated with the role (string, optional)

    allow_device_groups
        List of device groups to allow access to (string or list, optional. Use "All" for access
        to all device groups)

    allow_ip_groups
        List of IP groups to allow access to (string or list, optional. Use "All" for access
        to all IP groups)

    allow_servers
        List of Wanguard servers to allow access to (string or list, optional. Use "All" for access
        to all Wanguard servers)

    read_only_dashboards
        List of dashboard to allow read-only access to (string or list, optional. Use "All" for
        read-only access to all Wanguard dashboards)

    dashboards_access_policy
        The dashboard access policy. (one of: "No modification allowed",
        "Allow modifications to Full-access Dashboards",
        "llow modifications to Full-access Dashboards and permit adding new ones")

    full_dashboards
        Dashboards to allow full access to (string or list, optional. Use "All" for
        full access to all Wanguard dashboards, or "None" for no full access.)

    reports_tools
        Show tools reports (one of: "Show", "Hide")

    reports_tools_items
      List of tools reports to grant access to. (one or more of: "Anomalies", "Anomalies Actions",
      "Routing", "Routing Actions", "Firewall", "Firewall Actions", "Flows", "Packets",
      "Packets Actions & Captures", "Packets Actions"; optional)

    reports_devices
        Show device reports (one of: "Show", "Hide")

    reports_dashboards
        Show dashboard reports (one of: "Show", "Hide")

    reports_ip_addresses
        Show IP addresses reports (one of: "Show", "Hide")

    reports_ip_groups
        Show IP groups reports (one of: "Show", "Hide")

    reports_servers
        Show servers reports (one of: "Show", "Hide")

    ipzones_access_policy
        IP zones access policy (one of: "No access",
        "Allow full access to the allowed IP Group(s) in the selected IP Zone(s)")

    ipzones
        IP zones to grant access to (string or list, optional)

    responses_access_policy
        Responses access policy (one of: "No access",
        "Allow using the selected Response(s) when defining thresholds",
        "Allow full access to the selected Response(s)",
        "Allow full access to the selected Response(s) and permit adding new ones")

    responses
        Responses to grant access to (string or list, optional)

    response_actions
        Response actions to grant access to (one or more of:
        "Announce the destination IP address in a BGP routing update",
        "Capture a sample of malicious packets",
        "Detect filtering rules and mitigate the attack with Wanguard Filter",
        "Execute a command or script with dynamic parameters as arguments",
        "Send a custom email notification", "Send a custom Syslog message",
        "Send a custom SNMP trap",
        "Send a visual or audio notification to all logged-in Console users",
        "Generate an anomaly report and send it by email",
        "Announce a BGP routing update with Flowspec or S/RTBH",
        "Apply the filtering rule on a third-party inline device",
        "Capture a sample of packets matched by the filtering rule"; or "All")

    threshold_templates_access_policy
        Threshold templates access policy (one of:
        "No access", "Allow read-only access to the selected Threshold Template(s)",
        "Allow full access to the selected Threshold Template(s)",
        "Allow full access to the selected Threshold Template(s) and permit adding new ones")

    threshold_templates
        Threshold templates to grant access to (string or list, optional)

    whitelist_templates_access_policy
        Whitelist templates access policy (one of: "No access",
        "Allow read-only access to the selected Whitelist Template(s)",
        "Allow limited access to the selected Whitelist Template(s)"

    whitelist_templates
        Whitelist templates to grant access to (string or list, optional)

    scheduled_reports_access_policy
        Scheduled reports access policy (one of: "No access",
        "Allow full access to the selected Scheduled Report(s) and permit adding new ones")

    scheduled_reports
        Scheduled reports to grant access to (string or list, optional)

    profile
        Profile to use (string, optional)

    CLI Example:

    .. code-block:: yaml

        Wanguard Guest Role Present:
          wanguard.role_present:
            - name: Guest
            - description: Read-only access
            - south_region: Show
            - help_menu: Show
            - comments: >
                This role is managed by Salt. Any changes to this role may be overwritten
                automatically and without warning.
            - allow_device_groups: All
            - allow_ip_groups: All
            - allow_servers: All
            - read_only_dashboards: All
            - dashboards_access_policy: No modifications allowed
            - full_dashboards: None
            - reports_tools: Show
            - reports_tools_items:
                - Anomalies
                - Anomalies Actions
                - Routing
                - Routing Actions
                - Firewall
                - Firewall Actions
                - Flows
                - Packets
                - Packets Actions & Captures
                - Packets Actions
            - reports_devices: Show
            - reports_dashboards: Show
            - reports_ip_addresses: Show
            - reports_ip_groups: Show
            - reports_servers: Show
            - ipzones_access_policy: No access
            - ipzones: All
            - responses_access_policy: No access
            - responses: All
            - response_actions: All
            - threshold_templates_access_policy: Allow read-only access to the selected Threshold Template(s)
            - threshold_templates: All
            - whitelist_templates_access_policy: Allow read-only access to the selected Whitelist Template(s)
            - whitelist_templates: All
            - scheduled_reports_access_policy: No access
            - scheduled_reports: All
    """
    opts = _get_config(profile=profile)
    role = wanguard_util.get_role(opts=opts, role_name=name)
    if not role:
        log.debug('%s Role "%s" doesn\'t exist', LOG_PREFIX, name)
        wanguard_util.create_role(
            opts=opts,
            description=description,
            south_region=south_region,
            help_menu=help_menu,
            comments=comments,
            allow_device_groups=allow_device_groups,
            allow_ip_groups=allow_ip_groups,
            allow_servers=allow_servers,
            read_only_dashboards=read_only_dashboards,
            dashboards_access_policy=dashboards_access_policy,
            full_dashboards=full_dashboards,
            reports_tools=reports_tools,
            reports_tools_items=reports_tools_items,
            reports_devices=reports_devices,
            reports_dashboards=reports_dashboards,
            reports_ip_addresses=reports_ip_addresses,
            reports_ip_groups=reports_ip_groups,
            reports_servers=reports_servers,
            ipzones_access_policy=ipzones_access_policy,
            ipzones=ipzones,
            responses_access_policy=responses_access_policy,
            responses=responses,
            response_actions=response_actions,
            threshold_templates_access_policy=threshold_templates_access_policy,
            threshold_templates=threshold_templates,
            whitelist_templates_access_policy=whitelist_templates_access_policy,
            whitelist_templates=whitelist_templates,
            scheduled_reports_access_policy=scheduled_reports_access_policy,
            scheduled_reports=scheduled_reports,
        )
    else:
        log.debug('%s Role "%s" exists', LOG_PREFIX, name)
        proposed_role = {
            "role_name": name,
            "description": description,
            "south_region": south_region,
            "help_menu": help_menu,
            "comments": comments,
            "allow_device_groups": allow_device_groups,
            "allow_ip_groups": allow_ip_groups,
            "allow_servers": allow_servers,
            "read_only_dashboards": read_only_dashboards,
            "dashboards_access_policy": dashboards_access_policy,
            "full_dashboards": full_dashboards,
            "reports_tools": reports_tools,
            "reports_tools_items": reports_tools_items,
            "reports_devices": reports_devices,
            "reports_dashboards": reports_dashboards,
            "reports_ip_addresses": reports_ip_addresses,
            "reports_ip_groups": reports_ip_groups,
            "reports_servers": reports_servers,
            "ipzones_access_policy": ipzones_access_policy,
            "ipzones": ipzones,
            "responses_access_policy": responses_access_policy,
            "responses": responses,
            "response_actions": response_actions,
            "threshold_templates_access_policy": threshold_templates_access_policy,
            "threshold_templates": threshold_templates,
            "whitelist_templates_access_policy": whitelist_templates_access_policy,
            "whitelist_templates": whitelist_templates,
            "scheduled_reports_access_policy": scheduled_reports_access_policy,
            "scheduled_reports": scheduled_reports,
        }
        result = dictdiffer.diff(role, proposed_role)
        log.debug('%s Dict diff "%s"', LOG_PREFIX, result)
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    return ret


def role_absent(name):
    """
    Ensure the named role is absent in Wanguard

    name
        Name of the role (string)

    CLI Example:

    .. code-block:: yaml

        Wanguard Guest Role Absent:
          wanguard.role_absemt:
            - name: Guest
    """
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    value = __salt__["wanguard.role_absent"](name)
    if value == name:
        ret["result"] = True
        ret["comment"] = f"The 'wanguard.role_absent' returned: '{value}'"
    return ret


def user_present(name):
    """
    Ensure the named user is present in Wanguard

    name
        Name of the user (string)

    role
        Role to be assigned to user (string)

    full_name
        The full name of the user (string)

    authentication
        The authentication type to be used for the user (one of: "Local Password",
        "Remote Authentication")

    password
        The password for the user (string; not used if "Remote Authentication is selected for
        `authentication`)

    company
        The organization the user belongs to (string, optional)

    position
        The user's position (string, optional)

    email
        The user's email address (string, optional)

    landing_tab_id
        Shows the tab that opens immediately after logging in (string)

    landing_tab_name
        Figure this out later (string)

    console_theme
        The theme to use for the user's console (one of: "Classic Dark", "Classic Blue",
        "Classic Gray", "Modern Dark", "Modern Blue", "Modern Light Gray", "Modern Dark Gray",
        "Modern Navy", "Modern Orange", "Modern White")

    console_iconset
        The icon set to use for the user's console (one of: "Auto", "Classic", "Modern"; optional)

    reports_region
        The region of the console to use for reports (one of: "West", "East")

    configuration_region
        The region of the console to use for reports (one of: "West", "East")

    mininum_severity
        Minimum severity level of the events displayed in user's console (one of: "MELTDOWN",
        "CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG")

    console_notifications
        Controls the visual and audio notifications sent to the user via responses (one of:
        "Disabled", "Visual", "Audio", "Visual & Audio")

    rest_api_access:
        Controls whether the user has access to the REST API using their credentials (one of:
        "Disabled", "Enabled")

    comments
        Comments associated with the user (string, optional)

    CLI Example:

    .. code-block:: yaml

        Wanguard Jane Doe User Present:
          wanguard.user_present:
            - name: jdoe
            - role: Guest
            - full_name: Jane Done
            - authentication: Local Password
            - password: CorrectHorseBatteryStaple
            - company: Acme Corporation
            - position: Director of Widget Production
            - email: jdoe@example.com
            - landing_tab_id: Figure this out later
            - landing_tab_name: Figure this out later
            - console_theme: Modern Dark
            - console_iconset: Auto
            - reports_region: West
            - configuration_region: East
            - minimum_severity: INFO
            - console_notifications: Disabled
            - rest_api_access: Disabled
            - comments: >
                This user is managed by Salt. Any changes to this user may be overwritten
                automatically and without warning.
    """
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    value = __salt__["wanguard.user_present"](name)
    if value == name:
        ret["result"] = True
        ret["comment"] = f"The 'wanguard.user_present' returned: '{value}'"
    return ret


def user_absent(name):
    """
    Ensure the named user is absent in Wanguard

    name
        Name of the user (string)

    CLI Example:

    .. code-block:: yaml

        Wanguard Jane Doe User Absent:
          wanguard.user_absent:
            - name: jdoe
    """
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    value = __salt__["wanguard.user_absent"](name)
    if value == name:
        ret["result"] = True
        ret["comment"] = f"The 'wanguard.user_absent' returned: '{value}'"
    return ret


def ip_zone_present(name):
    """
    Ensure the named IP zone is present in Wanguard

    name
        Name of the IP zone (string)

    CLI Example:

    .. code-block:: yaml

        Wanguard IP Zone Default Present:
          wanguard.ip_zone_present:
            - name: IP Zone Default
    """
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    value = __salt__["wanguard.ip_zone_present"](name)
    if value == name:
        ret["result"] = True
        ret["comment"] = f"The 'wanguard.ip_zone_present' returned: '{value}'"
    return ret


def ip_zone_absent(name):
    """
    Ensure the named IP zone is absent in Wanguard

    name
        Name of the IP zone (string)

    CLI Example:

    .. code-block:: yaml

        Wanguard IP Zone Default Absent:
          wanguard.ip_zone_absent:
            - name: IP Zone Default
    """
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    value = __salt__["wanguard.ip_zone_absent"](name)
    if value == name:
        ret["result"] = True
        ret["comment"] = f"The 'wanguard.ip_zone_absent' returned: '{value}'"
    return ret


def ip_zone_prefix_present(name):
    """
    Ensure the named IPv4/IPv6 prefix is present in an IP zone

    name
        Name of the IP zone (string)

    prefix
        The IPv4/IPv6 prefix (string)

    ip_group
        The IP group the prefix should belong to

    ip_graphing
        Enable IP graphing for the prefix (one of: "Inherit", "No", "Yes"; optional)

    ip_accounting
        Enable IP accounting for the prefix (one of: "Inherit", "No", "Yes"; optional)

    thresholds_template
        Threshold template to assign to the prefix (string, optional)

    profiling
        Enable profiling for the prefix (one of: "Inherit", "No", "Subnet", "IPs", "Subnet + IPs";
        optional)

    profiling_response
        Profiling response to assign to the prefix (string, optional)

    comments
        Comments associated with the prefix (string, optional)

    CLI Example:

    .. code-block:: yaml

        Wanguard 192.0.2.0/24 Prefix in IP Zone Default Present:
          wanguard.ip_zone_prefix_present:
            - name: IP Zone Default
            - prefix: 192.0.2.0/24
            - ip_group: Documentation (TEST-NET-1)
            - ip_graphing: Yes
            - ip_accounting: No
            - thresholds_template: Thresholds Default
            - profiling: Subnet + IPs
            - profiling_response: Response Default
            - comments:  >
                This prefix is managed by Salt. Any changes to this prefix may be overwritten
                automatically and without warning.
    """
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    value = __salt__["wanguard.ip_zone_prefix_present"](name)
    if value == name:
        ret["result"] = True
        ret["comment"] = f"The 'wanguard.ip_zone_prefix_present' returned: '{value}'"
    return ret


def ip_zone_prefix_absent(name, prefix):
    """
    Ensure the named IPv4/IPv6 prefix is absent in an IP zone

    name
        Name of the IP zone (string)

    prefix
        The IPv4/IPv6 prefix (string)

    CLI Example:

    .. code-block:: yaml

        Wanguard 192.0.2.0/24 Prefix in IP Zone Default Absent:
          wanguard.ip_zone_prefix_absent:
            - name: IP Zone Default
            - prefix: 192.0.2.0/24
    """
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    value = __salt__["wanguard.ip_zone_prefix_absent"](name, prefix)
    if value == name:
        ret["result"] = True
        ret["comment"] = f"The 'wanguard.ip_zone_prefix_absent' returned: '{value}'"
    return ret


def threshold_template_present(name):
    """
    Ensure the named threshold template is present

    name
        Name of the threshold template (string)

    comments
        Comments associated with the threshold template (string, optional)

    thresholds
        A list of thresholds configured on the threshold template. The threshold dictionary format
        is:

        domain
            The domain the threshold is associated with (one of: "Internal IP", "Subnet")

        direction
            The traffic direction for the threshold (one of "receives", "sends")

        comparison
            The comparison operator for the threshold (one of "over", "under")

        value
            The value of the traffic (integer, optionally suffixed by one of "K", "M", "G"; or "Unlimited")

        decoder
            Name of traffic decoder (string)

        unit
            The units that `value` represents (one of: "pkt/s", "bits/")

        response
            Response to assign to the prefix (string, "None" if no response)

        parent
            Allow the threshold to be inherited by more specific prefixes (boolean)

    CLI Example:

    .. code-block:: yaml

        Wanguard Thresholds Default Present:
          wanguard.threshold_template_present:
            - name: Thresholds Default
            - comments:  >
                This threshold template is managed by Salt. Any changes to this threshold template may be overwritten
                automatically and without warning.
            - thresholds:
                - domain: Internal IP
                  direction: receives
                  comparison: over
                  value: 15K
                  decoder: IP
                  unit: pkts/s
                  response: Response Default
                  parent: yes
    """
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    value = __salt__["wanguard.threshold_template_present"](name)
    if value == name:
        ret["result"] = True
        ret["comment"] = f"The 'wanguard.threshold_template_present' returned: '{value}'"
    return ret


def threshold_template_absent(name, prefix):
    """
    Ensure the named threshold_template is absent

    name
        Name of the threshold_template (string)

    CLI Example:

    .. code-block:: yaml

        Wanguard Thresholds Default Absent:
          wanguard.threshold_template_present:
            - name: Thresholds Default
    """
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    value = __salt__["wanguard.threshold_template_absent"](name, prefix)
    if value == name:
        ret["result"] = True
        ret["comment"] = f"The 'wanguard.threshold_template_absent' returned: '{value}'"
    return ret


def response_present(name):
    """
    Ensure the named response is present

    name
        Name of the response (string)

    actions
        A list of actions configured on the response. The action dictionary format is:

        response_branch
            The response branch type (one of: "When an anomaly is detected",
            "When a filtering rule is detected", "When an anomaly expires",
            "When a filtering rule expires")

        The following values are common to all response branches:

        action_type
            The type of action to take (one of:
            "Announce the destination IP address in a BGP routing update",
            "Execute a command or script with dynamic parameters as arguments",
            "Send a custom email notification",
            "Detect filtering rules and mitigate the attack with Wanguard Filter",
            "Send a custom Syslog message", "Capture a sample of malicious packets",
            "Capture a sample of packets matched by the filtering rule",
            "Announce a BGP routing update with FlowSpec or S/RTBH",
            "Generate an anomaly report and send it by email", "Send a custom SNMP trap",
            "Apply the filtering rule on a third-party inline device",
            "Delete the filtering rule on a third-party inline device")

        action_name
            The name of the action (string)

        action_priority
            The order of execution relative to the other actions defined within the same panel.
            Lower numerical values correspond to increased priority (1 <= integer <= 9)

        action_periodicity
            Actions can be executed once per anomaly/filtering rule, or periodically. The frequency
            of execution is 5 seconds for Packet Sensor, Packet Filter, Sensor Cluster, and Filter
            Cluster, or 5-60 seconds for Flow Sensor, depending upon its Granularity parameter
            (one of: "Run Once", "Run Periodically")

        action_execution
            Actions can be executed automatically without end-user intervention or manually by an
            Operator or Administrator (one of: "Automatic", "Manual")

        action_log
            When enabled, the name of the action is recorded and displayed in anomaly reports
            (boolean)

        preconditions_policy
            Policy that determines which conditions must be true before the action is executed (one
            of: "All True", "Any True"; optional)

        preconditions
            A list of preconditions configured on the action. The precondition dictionary format
            is:

            dynamic_parameter
                The parameter to perform comparison against (one of: "prefix", "ip", "cidr",
                "ip_group", "sensor", "sensor_group", "sensor_ip", "sensor_type", "sensor_id",
                "router_ip", "ipzone", "prefix_ipzone", "response", "response_actions", "template",
                "expiration", "captured_pkts", "bgplog_bytes", "classification", "exclusive",
                "custom_script", "anomaly", "anomaly_id", "classification", "comment", "direction",
                "domain", "class", "threshold_type", "decoder", "operation", "unit", "rule_value",
                "computed_threshold", "anomaly_pps", "anomaly_bps", "latest_anomaly_pps",
                "latest_anomaly_bps", "value", "latest_value", "sum_value", "severity",
                "latest_severity", "link_severity", "latest_link_severity",
                "latest_link_utilization", "total_pps", "total_bps", "latest_total_pps",
                "latest_total_bps", "sum_total_pkts", "sum_total_bits", "from", "from_unixtime",
                "from_minute", "from_hour", "from_day", "from_dow", "from_month", "from_year",
                "until", "until_unixtime", "until_minute", "until_hour", "until_day", "until_dow",
                "until_month", "until_year", "duration", "tick", "filter", "filter_group",
                "filter_type", "filter_id", "filtering_rule_id", "filtering_rule_type",
                "filtering_rule_value", "filtering_rule_ip_isp", "filtering_rule_ip_country",
                "filtering_rule_pps", "filtering_rule_bps", "filtering_rule_max_pps",
                "filtering_rule_max_bps", "filtering_rule_unit", "filtering_rule_max_unit",
                "filtering_rule_packets", "filtering_rule_bits", "filtering_rule_difftime",
                "filtering_rule_severity", "filtering_rule_whitelisted", "filtering_rule_log_size",
                "filters", "filters_pps", "filters_bps", "filters_max_pps", "filters_max_bps",
                "filters_filtered_packets", "filters_filtered_bits", "filters_max_cpu_usage",
                "filters_ips")

            operator
                The operator used to perform the comparison (one of: "equal", "not equal",
                "less than", "greater than", "divisible by", "includes", "excludes", "included in",
                "excluded from", "regexp")

            value
                The value against which to compare the dynamic parameter (string)

        The following values apply to the
        `Announce the destination IP address in a BGP routing update` reponse branch:

        bgp_connector
            The BGP connector from which to send/withdraw the announcment (string)

        bgp_action
           The BGP announcement action (one of: "Send announcement", "Withdraw announcement")

        bgp_withdraw_after
           Number of minutes after which to withdraw the BGP announcement. Set to 0 to withdraw
           when the anomaly expires (integer >= 0)

        override1
           The BGP attribute to modify as part of the announcement (one of: "None", "AS Number",
           "Route Map", "Community", "Extended Community", "Large Community", "Redirect (IP/VRF)",
           "Other Parameter"; optional)

        override1_value
            The value for the BGP attribute specified in `override` (string, optional)

    CLI Example:

    .. code-block:: yaml

        Wanguard Response Default Present:
          wanguard.response_present:
            - name: Response Default
            - actions:
                - response_branch: When an anomaly is detected
                  action_type: Announce the destination IP address in a BGP routing update
                  action_name: Acme AS65551 RTBH
                  action_priority: 9
                  action_periodicity: Run Once
                  action_execution: Automatic
                  action_log: True
                  preconditions_policy: Any True
                  preconditions:
                    - dynamic_parameter: sensor
                      operator: equal
                      value: router1
                    - dynamic_parameter: sensor
                      operator: equal
                      value: router2
                  bgp_connector: Server1 BGP Connector
                  bgp_action: Send announcement
                  bgp_withdraw_after: 0
                  override1: Large Community
                  override1_value: 65551:65536:666
    """
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    value = __salt__["wanguard.response_present"](name)
    if value == name:
        ret["result"] = True
        ret["comment"] = f"The 'wanguard.response_present' returned: '{value}'"
    return ret


def response_absent(name, prefix):
    """
    Ensure the named reponse is absent

    name
        Name of the response (string)
    """
    ret = {"name": name, "changes": {}, "result": False, "comment": ""}
    value = __salt__["wanguard.response_absent"](name, prefix)
    if value == name:
        ret["result"] = True
        ret["comment"] = f"The 'wanguard.response_absent' returned: '{value}'"
    return ret

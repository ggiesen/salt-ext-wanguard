.. all-saltext.wanguard.modules:

_________________
Execution Modules
_________________

.. currentmodule:: saltext.wanguard.modules

.. autosummary::
    :toctree:

    wanguard_mod

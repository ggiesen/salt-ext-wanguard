``saltext-wanguard``: Integrate Salt with Wanguard
==================================================

Salt Extension for interacting with Andrisoft Wanguard

.. toctree::
  :maxdepth: 2
  :caption: Guides
  :hidden:

  topics/installation

.. toctree::
  :maxdepth: 2
  :caption: Provided Modules
  :hidden:

  ref/modules/index
  ref/runners/index
  ref/states/index

.. toctree::
  :maxdepth: 2
  :caption: Reference
  :hidden:

  changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
